"""Hackaton URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# from django.contrib import admin
from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from AppHackaton import views

urlpatterns = [
    # path('admin/', admin.site.urls),
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),
    path('materiales/<int:pk>/', views.MaterialesDetailView.as_view()),
    path('materiales/', views.MaterialesListView.as_view()),
    path('premio/', views.PremioListView.as_view()),
    path('materiales/new/', views.MaterialesCreateView.as_view()),
    path('premio/new/', views.PremioCreateView  .as_view()),
    path('materialesUD/<int:pk>/', views.MaterialesUpdateDeleteView.as_view()),
    path('premioUD/<int:pk>/', views.PremioUpdateDeleteView.as_view()),
]
