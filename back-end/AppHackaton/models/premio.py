from django.db import models

class Premio(models.Model):
    id = models.AutoField(primary_key=True)
    nombre_premio = models.CharField('Premio', max_length=100)
    img_premio = models.CharField('Imagen', max_length=100)
    puntos = models.IntegerField('Puntos')