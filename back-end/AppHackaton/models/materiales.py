from django.db import models

class Materiales(models.Model):
    id = models.AutoField(primary_key=True)
    tipo_material = models.CharField('Tipo', max_length=20)
    puntos_material = models.IntegerField('Puntos')
    reciclable = models.BooleanField('Reciclable', default=False)
    img_material1 = models.CharField('Imagen1', max_length=100)
    img_material2 = models.CharField('Imagen2', max_length=100)
    cantidad_material = models.DecimalField('Cantidad', max_digits=15, decimal_places=2)
    unidades_medida = models.CharField('Unidades', max_length=20)
    descripcion = models.CharField('Descripcion', max_length=500)
    contenedor_correcto = models.CharField('Contenedor', max_length=20)