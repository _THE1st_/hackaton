# Generated by Django 4.0 on 2021-12-12 15:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('AppHackaton', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Materiales',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('tipo_material', models.CharField(max_length=20, verbose_name='Tipo')),
                ('puntos_material', models.IntegerField(verbose_name='Puntos')),
                ('reciclable', models.BooleanField(default=False, verbose_name='Reciclable')),
                ('img_material1', models.CharField(max_length=100, verbose_name='Imagen1')),
                ('img_material2', models.CharField(max_length=100, verbose_name='Imagen2')),
                ('cantidad_material', models.DecimalField(decimal_places=2, max_digits=15, verbose_name='Cantidad')),
                ('unidades_medida', models.CharField(max_length=20, verbose_name='Unidades')),
                ('descripcion', models.CharField(max_length=500, verbose_name='Descripcion')),
                ('contenedor_correcto', models.CharField(max_length=20, verbose_name='Contenedor')),
            ],
        ),
        migrations.CreateModel(
            name='Premio',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('nombre_premio', models.CharField(max_length=100, verbose_name='Premio')),
                ('img_premio', models.CharField(max_length=100, verbose_name='Imagen')),
                ('puntos', models.IntegerField(verbose_name='Puntos')),
            ],
        ),
    ]
