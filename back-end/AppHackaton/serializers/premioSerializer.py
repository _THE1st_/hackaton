from rest_framework import serializers
from AppHackaton.models.premio import Premio

class PremioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Premio
        fields = '__all__'