from rest_framework import serializers
from AppHackaton.models.materiales import Materiales

class MaterialesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Materiales
        fields = '__all__'