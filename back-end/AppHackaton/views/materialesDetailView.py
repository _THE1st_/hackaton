from rest_framework import generics, response, status
from AppHackaton.models.materiales import Materiales
from AppHackaton.serializers.materialesSerializer import MaterialesSerializer

class MaterialesListView(generics.ListAPIView):
    serializer_class = MaterialesSerializer

    def get_queryset(self):
        queryset=Materiales.objects.all()
        return queryset

class MaterialesDetailView(generics.RetrieveAPIView):
    queryset = Materiales.objects.all()
    serializer_class = MaterialesSerializer

    def get(self, request, *args, **kwargs):
        return super().get(self,request,*args,**kwargs)


class MaterialesUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Materiales.objects.all()

    serializer_class=MaterialesSerializer

    # def delete(self, request, *args, **kwargs):
    #     super().destroy(request, *args, **kwargs)
    #     return response("El elemento fue eliminado con éxito", status=status.HTTP_200_OK)