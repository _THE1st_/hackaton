from rest_framework import generics
from AppHackaton.models.premio import Premio
from AppHackaton.serializers.premioSerializer import PremioSerializer

class PremioListView(generics.ListAPIView):
    serializer_class = PremioSerializer

    def get_queryset(self):
        queryset=Premio.objects.all()
        return queryset

# class PremioDetailView(generics.RetrieveAPIView):
#     queryset = Premio.objects.all()
#     serializer_class = PremioSerializer

#     def get(self, request, *args, **kwargs):
#         return super().get(self,request,*args,**kwargs)

class PremioUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Premio.objects.all()

    serializer_class=PremioSerializer