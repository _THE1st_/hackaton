from rest_framework import generics, status
from rest_framework.response import Response
from AppHackaton.serializers import MaterialesSerializer

class MaterialesCreateView(generics.CreateAPIView):
    serializer_class=MaterialesSerializer
    def post(self,request,*args,**kwargs):
        serializer=MaterialesSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response("Elemento creado", status=status.HTTP_201_CREATED)