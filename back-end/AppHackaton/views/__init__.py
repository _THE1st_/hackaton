from .userCreateView import UserCreateView
from .userDetailView import UserDetailView
from .materialesDetailView import MaterialesDetailView
from .materialesDetailView import MaterialesListView
# from .premioDetailView import PremioDetailView
from .premioDetailView import PremioListView
from .materialesCreateView import MaterialesCreateView
from .premioCreateView import PremioCreateView
from .materialesDetailView import MaterialesUpdateDeleteView
from .premioDetailView import PremioUpdateDeleteView