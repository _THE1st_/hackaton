from django.apps import AppConfig


class ApphackatonConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'AppHackaton'
